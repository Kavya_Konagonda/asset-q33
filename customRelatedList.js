import {
    LightningElement,
    track,
    api
} from 'lwc';
import getContacts from '@salesforce/apex/CustomRelatedListCtrl.getContacts';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

export default class CustomRelatedList extends NavigationMixin(LightningElement) {
    @api recordId;
    @track showSpinner = false;
    @track data = [];
    @track noOfContacts = 0;

    connectedCallback() {
        getContacts({
                accId: this.recordId

            })
            .then(result => {
                if (result.length > 0) {
                    this.data = result
                    this.noOfContacts = result.length
                } else {
                    this.data = ''
                    this.noOfContacts = 0
                }

            })
            .catch(error => {
                this.showToastMessage("Error!", JSON.stringify(error), "error");
            });

    }
    redirect(event){
        
        let contactId = this.data[event.target.dataset.index].Id;

        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: contactId,
                actionName: 'view'
            }
        }).then(url => {
            window.open(url, "_blank");
        });
    }
    showToastMessage(title, message, variant) {
        const event = new ShowToastEvent({
            "title": title,
            "message": message,
            "variant": variant,
        });
        this.dispatchEvent(event);
    }

}