/**
 * @description       : 
 * @author            : kavya.konagonda@mtxb2b.com
 * @group             : 
 * @last modified on  : 09-30-2021
 * @last modified by  : kavya.konagonda@mtxb2b.com
**/
public with sharing class CustomRelatedListCtrl {
    
    @AuraEnabled
    public static List<Contact> getContacts(String accId){
        List<Contact> contactList =  new List<Contact>();
        for(Contact con : [SELECT Id,Name,Email,Account.Name,Phone FROM Contact WHERE AccountId =:accId WITH SECURITY_ENFORCED ORDER BY FirstName]){
                                                    contactList.add(con);
            }
        
        return contactList;
    }
}
    
    